; given number of frames, returns a (truncated) corresponding num. of milliseconds
; I use this extensively in the Melee actions framework
framesToMs(f, floorIt)
{
    toMs := f * 16.666
    If (floorIt)
        toMs := floor(toMs)
    Else
        toMs := ceil(toMs)
    Return toMs
}

; only works for one action
HoldFor(action, frames)
{
    ; MsgBox, Performing %action% for %frames% frames
    Hold%action%()
    Sleep, framesToMs(frames, true)
    Release%action%()
}

; example: if prob = 0.3, then return true in 30% of calls
getBoolFromProb(prob)
{
    chance := 0.
    Random, chance, 0., 1.
    if (chance < prob)
        return false
    else
        return true
}
