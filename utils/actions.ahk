#include %A_LineFile%\..\controller.ahk
#include %A_LineFile%\..\action-macros.ahk

; Remember to release Ctrl stick right!
JumpRight(af)
{
    SendInput, {Space Down}
    SendInput, {d Down}
    Sleep, framesToMs(2, false) ; make certain jump registers
    SendInput, {Space Up}
    Sleep, framesToMs(af - 2, true)
}

; Remember to release Ctrl stick left!
JumpLeft(af)
{
    SendInput, {Space Down}
    SendInput, {a Down}
    Sleep, framesToMs(2, false) ; make certain jump registers
    SendInput, {Space Up}
    Sleep, framesToMs(af - 2, true)
}

; pass the frame the character becomes airborne -- e.g., for Fox, 4; for Marth, 5
Wavedash(af, isLeft)
{
    if (isLeft)
        HoldCtrlStickDiagDownLeft()
    else
        HoldCtrlStickDiagDownRight()
    Sleep, framesToMs(1, true)

    Jump(af)

    HoldFor("Shield", 2)

    if (isLeft)
        ReleaseCtrlStickDiagDownLeft()
    else
        ReleaseCtrlStickDiagDownRight()

    ; wait out the lag, with a safety frame
    Sleep, framesToMs(8, true) ; leaves a frame window but always works
}

WavedashLeft(af)
{
    Wavedash(af, true)
}

WavedashRight(af)
{
    Wavedash(af, false)
}

WaveDashDown(af)
{
    HoldCtrlStickDown()
    Jump(af)
    HoldFor("Shield", 2)
    ReleaseCtrlStickDown()
    Sleep, framesToMs(9, true)
}

JCGrab()
{
    SendInput, {Space Down}
    Sleep, framesToMs(2, true)
    SendInput, {u Down}
    Sleep, framesToMs(2, true)
    SendInput, {u Up}
    SendInput, {Space Up}
}

SetUpSettings()
{
    MsgBox, game configuration macro not yet implemented
}

Dash(min, max, isLeft)
{
    ThisDDLength := max
    if (min <> max)
        Random, ThisDDLength, min, max
    if (isLeft)
        HoldCtrlStickLeft()
    else
        HoldCtrlStickRight()

    Sleep, framesToMs(ThisDDLength, false)

    if (isLeft)
        ReleaseCtrlStickLeft()
    else
        ReleaseCtrlStickRight()
}

DelayNextInput()
{
    lengthOfWait := 0
    Random, lengthOfWait, 1, 2 ; chance of immediate followup
    Sleep, framesToMs(lengthOfWait, false)
}

DashLeft(min, max)
{
    Dash(min, max, true)
}

DashRight(min, max)
{
    Dash(min, max, false)
}

; remember to release CtrlStickDown!
DashCC(f)
{
    HoldCtrlStickLeft()
    Sleep, framesToMs(f, true)
    HoldCtrlStickDown()
    Sleep, framesToMs(2, true)
    ReleaseCtrlStickLeft()
}

DashDanceThisTurn(min, max, af, WDProb)
{
    if getBoolFromProb(WDProb)
        DashLeft(min, max)
    else
        WavedashLeft(af)

    DelayNextInput() ; simulates a longer dash length and provides mixup

    if getBoolFromProb(WDProb)
        DashRight(min, max)
    else
        WavedashRight(af)

    DelayNextInput()
}

; due to instant aerial this takes 36 (not 38) frames
RetreatingShortHopDoubleFair()
{
    DashLeft(17,17)
    JumpRight(5)
    HoldFor("CStickLeft", 2)
    Sleep, framesToMs(28, true)
    HoldFor("CStickLeft", 2)
    Sleep, framesToMs(3, true)
    HoldFor("Shield", 2) ; l cancel
    ReleaseCtrlStickRight()
    Sleep, framesToMs(12, true)
}

; no need to l cancel
RetreatingShortHopACNair()
{
    DashLeft(17,17)
    JumpRight(5)
    ReleaseCtrlStickRight()
    Attack()
    HoldCtrlStickRight()
    Sleep, framesToMs(19, true)
    HoldFor("CtrlStickDown", 2)
    Sleep, framesToMs(6, false)
    ReleaseCtrlStickRight()
    Sleep, framesToMs(10, true) ; leave some extra frames
    DashRight(10, 10)
}

DashCCDtilt()
{
    DashCC(16)
    Sleep, framesToMs(4, true)
    Attack()
    Sleep, framesToMs(25, true) ; account for hitlag
    ReleaseCtrlStickDown()
    ; return to original position
    DashRight(14, 14)
    DashLeft(3, 3)
    DashRight(14, 14)
}

DashJCGrab()
{
    DashLeft(15, 15)
    JCGrab()
    Sleep, framesToMs(30, true)
    DashRight(12, 12)
    DashLeft(3, 3)
    DashRight(12, 12)
}

DashDance(MinDDLength, MaxDDLength, AirborneFrame, WDProb, HitboxProb)
{
    if getBoolFromProb(HitboxProb)
    {
        DashDanceThisTurn(MinDDLength, MaxDDLength, AirborneFrame, WDProb)
    }
    else
    {
        chance := 0
        Random, chance, 0, 3

        if (chance = 0)
            RetreatingShortHopDoubleFair()
        else if (chance = 1)
            RetreatingShortHopACNair()
        else if (chance = 2)
            DashCCDtilt()
        else
            DashJCGrab()
    }
}

