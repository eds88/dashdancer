; Control Stick commands
HoldCtrlStickLeft()
{
    SendInput, {a Down}
}

HoldCtrlStickUp()
{
    SendInput, {w Down}
}

HoldCtrlStickRight()
{
    SendInput, {d Down}
}

HoldCtrlStickDown()
{
    SendInput, {s Down}
}

ReleaseCtrlStickLeft()
{
    SendInput, {a Up}
}

ReleaseCtrlStickUp()
{
    SendInput, {w Up}
}

ReleaseCtrlStickRight()
{
    SendInput, {d Up}
}

ReleaseCtrlStickDown()
{
    SendInput, {s Up}
}

HoldCtrlStickDiagDownLeft()
{
    HoldCtrlStickLeft()
    HoldCtrlStickDown()
}

HoldCtrlStickDiagDownRight()
{
    HoldCtrlStickRight()
    HoldCtrlStickDown()
}

HoldCtrlStickDiagUpLeft()
{
    HoldCtrlStickLeft()
    HoldCtrlStickUp()
}

HoldCtrlStickDiagUpRight()
{
    HoldCtrlStickRight()
    HoldCtrlStickUp()
}

ReleaseCtrlStickDiagDownLeft()
{
    ReleaseCtrlStickLeft()
    ReleaseCtrlStickDown()
}

ReleaseCtrlStickDiagDownRight()
{
    ReleaseCtrlStickRight()
    ReleaseCtrlStickDown()
}

ReleaseCtrlStickDiagUpLeft()
{
    ReleaseCtrlStickLeft()
    ReleaseCtrlStickUp()
}

ReleaseCtrlStickDiagUpRight()
{
    ReleaseCtrlStickRight()
    ReleaseCtrlStickUp()
}

; C-stick commands
HoldCStickLeft()
{
    SendInput, {Left Down}
}

HoldCStickUp()
{
    SendInput, {Up Down}
}

HoldCStickRight()
{
    SendInput, {Right Down}
}

HoldCStickDown()
{
    SendInput, {Down Down}
}

ReleaseCStickLeft()
{
    SendInput, {Left Up}
}

ReleaseCStickUp()
{
    SendInput, {Up Up}
}

ReleaseCStickRight()
{
    SendInput, {Right Up}
}

ReleaseCStickDown()
{
    SendInput, {Down Up}
}

HoldCStickDiagDownLeft()
{
    HoldCStickLeft()
    HoldCStickDown()
}

HoldCStickDiagDownRight()
{
    HoldCStickRight()
    HoldCStickDown()
}

HoldCStickDiagUpLeft()
{
    HoldCStickLeft()
    HoldCStickUp()
}

HoldCStickDiagUpRight()
{
    HoldCStickRight()
    HoldCStickUp()
}

ReleaseCStickDiagDownLeft()
{
    ReleaseCStickLeft()
    ReleaseCStickDown()
}

ReleaseCStickDiagDownRight()
{
    ReleaseCStickRight()
    ReleaseCStickDown()
}

ReleaseCStickDiagUpLeft()
{
    ReleaseCStickLeft()
    ReleaseCStickUp()
}

ReleaseCStickDiagUpRight()
{
    ReleaseCStickRight()
    ReleaseCStickUp()
}

HoldShield()
{
    SendInput, {i Down}
}

ReleaseShield()
{
    SendInput, {i Up}
}

Jump(af)
{
    SendInput, {Space Down}
    Sleep, framesToMs(2, false) ; lock input until jump finishes
    SendInput, {Space Up}
    Sleep, framesToMs(af - 2, true)
}

Attack()
{
    SendInput, {p Down}
    Sleep, framesToMs(2, false)
    SendInput, {p Up}
}

Taunt()
{
    SendInput, {1 Down}
    Sleep, framesToMs(2, true)
    SendInput, {1 Up}
    Sleep, framesToMs(40, true)
}

