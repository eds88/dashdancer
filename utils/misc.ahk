DashDancingThisTurn(DDProb)
{
    chance := 1.
    Random, chance, 0., 1.
    ; MsgBox, %chance%
    if (chance < DDProb)
        return false
    else
        return true
}

SetUpChar()
{
    ; fall through platform
    loop, 5 ; just to be safe
    {
        SendInput, {s Down} ; crouch
        Sleep, framesToMs(5, false)
        SendInput, {s Up} ; uncrouch
        Sleep, framesToMs(5, false)
    }
    ; roll to edge of stage
    SendInput {i Down} ;  shield
    Sleep, framesToMs(5, false)
    SendInput {Right Down} ; c-stick right (buffer roll)
    Sleep, 3500 ; should be enough
    SendInput {Right Up}
    SendInput {i Up}
    ; walk away from edge (so he won't just dd off)
    SendInput {LShift Down} ; reduces 'joystick' radius -- allows walking
    SendInput {a Down} ; walk left (w/o LShift down would dash left)
    Sleep, framesToMs(70, false) ; leave lots of room
    SendInput {a Up}
    SendInput {LShift Up}
}

