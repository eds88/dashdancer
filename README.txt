Makes Marth dash dance, and occasionally throw safe hitboxes.

DEPENDENCIES
===
    - Dolphin (tested on 4.0.8863)
    - Super Smash Bros. Melee (20XX Hack Pack 4.0 beta preferred)

TODO
===
    - refactor to use vJoy interface instead of keyboard
    - [bug] allow sh double fair to throw second fair if first fair hits
    - [bug] stop Marth from running right into shield (why does this happen?)
    - [feature] add whiff punishes (retreating pivot grab, retreating pivot nair/fair)
    - [feature] support more characters
    - [feature] allow configuration without modifying the script (config file? GUI?)