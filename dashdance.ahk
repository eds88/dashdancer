#include %A_LineFile%\..\utils\actions.ahk
#include %A_LineFile%\..\utils\misc.ahk

; allow the app to exit (to deal with the infinite while loops)
Esc::ExitApp

; for testing (remove from release)
^t::
{
    HoldFor("CtrlStickLeft", 60)
    Return
}

^s::
{
    SetUpSettings()
    Return
}

^d::
{
    ; set up vars
    MinDDLength := 3 ; 1--2 frames occasionally pivots
    MaxDDLength := 15 ; false to prevent dashing too far
    WDProb := 0.34
    HitboxProb := 0.34
    AirborneFrame := 5

    ; prepare the character
    SetUpChar()
    Taunt()
    Sleep, framesToMs(60, true)

    ; dash dance
    loop
    {
        DashDance(MinDDLength, MaxDDLength, AirborneFrame, WDProb, HitboxProb)
    }
}
